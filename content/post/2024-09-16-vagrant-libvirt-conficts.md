---
title: "Vagrant error after Arch update"
date: 2024-09-18
tags:
- virtualization
- kvm
- libvirt
- vagrant
- arch
categories:
- blog
- sysadmin
- virtualization
author: jtorrex
---

# Vagrant error after Arch update

After applying updates to my Arch laptop, I experienced some issue while trying to run Vagrant.

First error was:

```
Unable to activate vagrant_cloud-3.1.1, because rexml-3.3.2 conflicts with rexml (~> 3.2.5)
```

Looking in GH I found this reference: https://github.com/hashicorp/vagrant/issues/13502

So I applied the downgrade to the Ruby package:

```
downgrade ruby-rexml
loading packages...
warning: downgrading package ruby-rexml (3.3.2-1 => 3.2.6-2)
resolving dependencies...
looking for conflicting packages...

Packages (1) ruby-rexml-3.2.6-2

Total Installed Size: 0.43 MiB
Net Upgrade Size: -0.57 MiB

:: Proceed with installation? [Y/n] y
(1/1) checking keys in keyring [#############################] 100%
(1/1) checking package integrity [#############################] 100%
(1/1) loading package files [#############################] 100%
(1/1) checking for file conflicts [#############################] 100%
(1/1) checking available disk space [#############################] 100%
:: Processing package changes...
(1/1) downgrading ruby-rexml [#############################] 100%
:: Running post-transaction hooks...
(1/1) Arming ConditionNeedsUpdate...
add ruby-rexml to IgnorePkg? [y/N] y
```

After downgrading the package I tried to expunge and reinstall the packages. 

```
vagrant plugin repair
vagrant plugin expunge --reinstall
vagrant plugin update
```

But another error appeared.

```
Vagrant failed to properly resolve required dependencies. These
errors can commonly be caused by misconfigured plugin installations
or transient network issues. The reported error is:

conflicting dependencies bigdecimal (= 3.1.3) and bigdecimal (= 3.1.8)
  Activated bigdecimal-3.1.8
  which does not match conflicting dependency (= 3.1.3)

  Conflicting dependency chains:
    bigdecimal (= 3.1.8), 3.1.8 activated

  versus:
    bigdecimal (= 3.1.3)

  Gems matching bigdecimal (= 3.1.3):
    bigdecimal-3.1.3

```
Retrying the process with the variable `VAGRANT_DISABLE_STRICT_DEPENDENCY_ENFORCEMENT=1 ` before applying the update, seems that works.

```
VAGRANT_DISABLE_STRICT_DEPENDENCY_ENFORCEMENT=1 vagrant plugin install vagrant-libvirt
```

And now Vagrant is fully functional again.

```
vagrant
Usage: vagrant [options] <command> [<args>]

    -h, --help                       Print this help.

Common commands:
     autocomplete    manages autocomplete installation on host
     box             manages boxes: installation, removal, etc.
     cloud           manages everything related to Vagrant Cloud
     destroy         stops and deletes all traces of the vagrant machine
     global-status   outputs status Vagrant environments for this user
     halt            stops the vagrant machine
     help            shows the help for a subcommand
     init            initializes a new Vagrant environment by creating a Vagrantfile
     login
     package         packages a running vagrant environment into a box
     plugin          manages plugins: install, uninstall, update, etc.
     port            displays information about guest port mappings
     powershell      connects to machine via powershell remoting
     provision       provisions the vagrant machine
     push            deploys code in this environment to a configured destination
     rdp             connects to machine via RDP
     reload          restarts vagrant machine, loads new Vagrantfile configuration
     resume          resume a suspended vagrant machine
     serve           start Vagrant server
     snapshot        manages snapshots: saving, restoring, etc.
     ssh             connects to machine via SSH
     ssh-config      outputs OpenSSH valid configuration to connect to the machine
     status          outputs status of the vagrant machine
     suspend         suspends the machine
     up              starts and provisions the vagrant environment
     upload          upload to machine via communicator
     validate        validates the Vagrantfile
     version         prints current and latest Vagrant version
     winrm           executes commands on a machine via WinRM
     winrm-config    outputs WinRM configuration to connect to the machine

For help on any individual command run `vagrant COMMAND -h`

Additional subcommands are available, but are either more advanced
or not commonly used. To see all subcommands, run the command
`vagrant list-commands`.
        --[no-]color                 Enable or disable color output
        --machine-readable           Enable machine readable output
    -v, --version                    Display Vagrant version
        --debug                      Enable debug output
        --timestamp                  Enable timestamps on log output
        --debug-timestamp            Enable debug output with timestamps
        --no-tty                     Enable non-interactive output
```

